#include "fps_core/spherical_projector.h"
#include "fps_core/depth_utils.h"

#include <fstream>

using namespace std;
using namespace fps_mapper;

int main(int argc, char** argv) {
  if (argc<1) {
    cerr << "usage: " << argv[0] << "<model1.dat>" << endl;
    return 0;
  }

  ifstream is1(argv[1]);
  if(! is1) {
    cerr << "unable to load file " << argv[1] << endl;
    return 0;
  }

  cerr << "loading model" << endl;
  Cloud model;
  model.read(is1);
  cerr << "reference has " << model.size() << " points" << endl;

  Eigen::Isometry3f iso;
  iso.setIdentity();
  Eigen::Vector3f angles(0,0,0);
  SphericalProjector projector;
  FloatImage zbuffer;
  IndexImage indices;
  bool ok=true;
  Eigen::Matrix3f bring_z_up;
  bring_z_up << 1,0,0,0,0,1,0,-1,0;
  iso.linear()=bring_z_up;
  model.transformInPlace(iso);
  UnsignedShortImage dest;
  while(ok) {
    cerr << "angles: " << angles.transpose() << endl;
    char key=cv::waitKey();
    switch(key) {
    case 'a':angles(0)+=0.1; break;
    case 's':angles(0)-=0.1; break;
    case 'w':angles(1)+=0.1; break;
    case 'z':angles(1)-=0.1; break;
    case 'p':cv::imwrite("dest.pgm", dest); break;
    case 'g': 
      {
	Cloud other_model;
	projector.unproject(other_model,dest);
	ofstream os("other_model.cloud");
	other_model.write(os);
      }
      break;
    
    case 27: ok=false; break;
    }
    cerr << "angles: " << angles.transpose() << endl;
    Eigen::Matrix3f R=
      Eigen::AngleAxisf(angles(0),Eigen::Vector3f(0,0,1)).matrix()*
      Eigen::AngleAxisf(angles(1),Eigen::Vector3f(0,1,0)).matrix();
    iso.linear()=R;
    projector.project(zbuffer, indices, iso, model);
    convert_32FC1_to_16UC1(dest, zbuffer, 1e3);
    cv::imshow("depth", dest);
  }
  return 0;

}
