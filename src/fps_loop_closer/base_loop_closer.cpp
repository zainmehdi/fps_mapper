#include "base_loop_closer.h"

namespace fps_mapper {

  BinaryNodeRelation* BaseLoopCloser::matchLocalMaps(LocalMap* target_local_map, LocalMap* source_local_map, 
						     Eigen::Isometry3f initial_guess, float voxel_leaf) {
    Cloud tmp_target(*target_local_map->cloud());
    Cloud tmp_source(*source_local_map->cloud());
    voxelize(tmp_target, voxel_leaf);
    voxelize(tmp_source, voxel_leaf);
    _aligner->setReferenceModel(&tmp_target);
    _aligner->setCurrentModel(&tmp_source);
    _aligner->align(initial_guess);
    return new BinaryNodeRelation(target_local_map, source_local_map, 
				  _aligner->T(), Matrix6f::Identity() * 1000.0f/*_aligner->informationMatrix()*/);
  }

}
