#include <sys/time.h>
#include <stdexcept>
#include <qapplication.h>
#include <qevent.h>

#include "boss/deserializer.h"
#include "boss/serializer.h"
#include "fps_core/nn_aligner.h"
#include "fps_map_viewers/trajectory_viewer.h"
#include "globals/system_utils.h"
#include "fps_global_optimization/g2o_bridge.h"

#include "trajectory_loop_closer.h"
#include "trajectory_energy_morphing_calculator.h"

using namespace fps_mapper;
using namespace boss;

const char* banner[] = {
  "fps_trajectory_loop_closer: perform loop closure on an input log",
  "usage:",
  " fps_trajectory_loop_closer [options] <input_log>",  
  " where: ",
  " -h                                  [], prints this help",
  " -adaptive_morphing              [bool], apply adaptive morphing when computing trajectory energy, default [true]",
  " -max_neighbors                   [int], maximum number of neighbors to consider for the loop closure, default [5]",
  " -refining_steps                  [int], number of refining alignments when registering two trajectories, default [1]",
  " -trajectory_iterations           [int], trajectories aligning iterations, default [10]",
  " -local_map_iterations            [int], local maps aligning iterations, default [20]",
  " -max_normal_angle              [float], max normal angle between two corresponding points in the local map alignment, default [PI/4]",
  " -max_point_displacement        [float], max distance between two corresponding points in the local map alignment, default [1.5]",
  " -max_energy                    [float], max energy for a local map trajectory to be considered a candidate for a closure, default [0.15]",
  " -max_energy_candidate_ratio    [float], maximum ratio of energy based candidates for the current local map, default [0.1]",
  " -max_distance                  [float], maximum distance radius where to search for neighbors candidates, default [2]",
  " -max_bad_point_distance        [float], maximum point distance for a point to be considered an inlier in the geometry check, default [0.075]",
  " -max_bad_point_ratio           [float], maximum bad point ratio for the geometry check, default [0.3]",
  " -max_projector_distance        [float], maximum projector distance used in the geometry check, default [2]",
  " -convergence_error             [float], convergence error threshold for trajectory alignment, default [0.00001]",
  " -pose_ratio_threshold          [float], pose ratio threshold between two local maps, default [0.3]",
  " -damping                       [float], damping factor for the trajectory aligner, default [100]",
  " -max_point_distance            [float], max distance for a correspondence in the trajectory alignment, default [1]",
  " -leaf_size                     [float], leaf size of the voxel grid applied to the local maps before registration, default [0.1]",
  " -straightness_min_log_ratio    [float], minimum threshold for which a trajectory is considered curved, default [0.005]",
  " -o                            [string], filename of the log where to save the map, default []",
  " <input_log>                   [string], input log",
  "",
  " while running:",
  " N: process next local map",
  " P: start auto local map processing",
  " B: stop auto local map processing",
  " C: clear all local maps",
  " V: show all local maps",
  " O: optimize the graph",
  " S: save the current map",
  " F: save screenshots",
  " K: print on the screen the camera position of the viewer",
  " H: print on the screen this help",
  0
};

inline double getTime() {
  struct timeval ts;
  gettimeofday(&ts, 0);
  return ts.tv_sec + ts.tv_usec * 1e-6;
}

class TrajectoryLoopCloserViewer: public TrajectoryViewer {
public:
  EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

  TrajectoryLoopCloserViewer(TrajectoryLoopCloser* trajectory_loop_closer_): TrajectoryViewer() {
    _need_redraw = true;
    _process_next = false;
    _process_automatically = false;
    _save_map = false;
    _save_screenshots = false;
    _counter = 0;
    _last_local_map = 0;
    _trajectory_loop_closer = trajectory_loop_closer_;
  }

  void init() {
    TrajectoryViewer::init();
    setAxisIsDrawn(true);
    setBackgroundColor(QColor(255, 255, 255));
    _need_redraw = true;
    showMaximized();
    // Top view for priscilla_catacombs_2015_01_13
    // camera()->setPosition(qglviewer::Vec(6.39692, 19.32750, 94.83940));
    // camera()->setOrientation(qglviewer::Quaternion(-0.22064, -0.16938, -0.26020, 0.92462));       
    // Top view for priscilla_catacombs_2015_08_06
    // camera()->setPosition(qglviewer::Vec(17.30388, -16.61296, 49.37919));
    // camera()->setOrientation(qglviewer::Quaternion(0.31452, -0.14645, 0.01776, 0.93772));        
    // Top view for diag_cellar_2014_12_11
    // camera()->setPosition(qglviewer::Vec(-6.6829, -6.95405, 41.4668));
    // camera()->setOrientation(qglviewer::Quaternion(0.0658999, -0.00624655, 0.729641, -0.680619));            
  }

  void keyPressEvent(QKeyEvent *e) {
    if(e->key() == Qt::Key_N) {
      _process_next = true;
    }
    else if(e->key() == Qt::Key_K) {
      std::cout << "[INFO] camera position " << camera()->position() << std::endl;
      std::cout << "[INFO] camera rotation " << camera()->orientation() << std::endl;
    }
    else if(e->key() == Qt::Key_H) {
      system_utils::printBanner(banner);
    }
    else if(e->key() == Qt::Key_P) {
      _process_automatically = true;
    }
    else if(e->key() == Qt::Key_B) {
      _process_automatically = false;
    }
    else if(e->key() == Qt::Key_C) {
      _selected_objects.clear();
    }
    else if(e->key() == Qt::Key_V) { 
      for(MapNodeList::iterator it = nodes.begin(); it!=nodes.end(); it++) { _selected_objects.insert(it->get()); }
    }
    else if(e->key() == Qt::Key_O) { 
      optimizeMap();
    }
    else if(e->key() == Qt::Key_S) {
      _save_map = true;
    }
    else if(e->key() == Qt::Key_F) {
      if(_save_screenshots) { _save_screenshots = false; } 
      else { _save_screenshots = true; } 
    }
    else {
      TrajectoryViewer::keyPressEvent(e);
    }
    _need_redraw = true;
  }

  virtual void draw() {    
    _need_redraw = false;

    // draw current local map
    if(_trajectory_loop_closer->targetNode()) {
      int attrs = ATTRIBUTE_SHOW;
      attrs |= ATTRIBUTE_SELECTED;
      glColor3f(0.0f, 0.0f, 1.0f);
      _trajectory_loop_closer->targetNode()->draw(attrs);
    }
    
    // draw accepted closure nodes
    for(MapNodeSet::iterator it = _trajectory_loop_closer->acceptedClosureNodes().begin(); 
    	it != _trajectory_loop_closer->acceptedClosureNodes().end(); 
    	++it) {
      MapNode* n = *it;
      int attrs = ATTRIBUTE_SHOW;
      attrs |= ATTRIBUTE_SELECTED;
      glColor3f(0.0f, 1.0f, 0.0f);
      n->draw(attrs);
    }

    // draw candidate closure nodes
    for(MapNodeSet::iterator it = _trajectory_loop_closer->candidateClosureNodes().begin(); 
    	it != _trajectory_loop_closer->candidateClosureNodes().end(); 
    	++it) {
      MapNode* n = *it;
      int attrs = ATTRIBUTE_SHOW;
      attrs |= ATTRIBUTE_SELECTED;
      attrs |= ATTRIBUTE_ONLY;      
      glColor3f(1.0f, 0.0f, 0.0f);
      n->draw(attrs);
    }

    TrajectoryViewer::draw();

    if(_save_screenshots) {
      char buffer[2048];
      sprintf(buffer, "snapshot_%05d.png", _counter);
      _counter++;
      this->setSnapshotFormat(QString("PNG"));
      this->setSnapshotQuality(10);
      this->saveSnapshot(QString(buffer), true);
    }
  }

  BinaryNodeRelationSet& optimizeMap() {
    relations.insert(_trajectory_loop_closer->acceptedClosureRelations().begin(), 
		     _trajectory_loop_closer->acceptedClosureRelations().end());
    _g2o_bridge.psToG2o(relations, nodes);
    _g2o_bridge.quietOptimize(25);
    _g2o_bridge.g2oToPs(nodes);
    return _trajectory_loop_closer->acceptedClosureRelations();
  }

  bool needRedraw() const { return _need_redraw; }
  bool processNext() const { return _process_next; }
  bool processAutomatically() const { return _process_automatically; }
  bool saveMap() const { return _save_map; }
  LocalMap* lastLocalMap() const { return _last_local_map; }
  const TrajectoryLoopCloser* trajectoryLoopCloser() const { return _trajectory_loop_closer; }

  void setNeedRedraw(const bool need_redraw_) { _need_redraw = need_redraw_; }
  void setProcessNext(const bool process_next_) { _process_next = process_next_; }
  void setProcessAutomatically(const bool process_automatically_) { _process_automatically = process_automatically_; }
  void setSaveMap(const bool save_map_) { _save_map = save_map_; }
  void setLastLocalMap(LocalMap* last_local_map_) { _last_local_map = last_local_map_; }
  void setTrajectoryLoopCloser(TrajectoryLoopCloser* trajectory_loop_closer_) { _trajectory_loop_closer = trajectory_loop_closer_; }

protected:
  bool _need_redraw, _process_next, _process_automatically, _save_map, _save_screenshots;
  int _counter;
  LocalMap* _last_local_map;
  G2OBridge _g2o_bridge;
  TrajectoryLoopCloser* _trajectory_loop_closer;
};

int main (int argc, char** argv) {
  // Corridor like environment
  bool adaptive_morphing = true;
  int max_neighbors = 5, refining_steps = 1, trajectory_iterations = 10, local_map_iterations = 20;
  float max_normal_angle = M_PI / 4.0f, max_point_displacement = 1.5f, max_energy = 0.15f, max_energy_candidate_ratio = 0.1;
  float max_distance = 2.0f, max_bad_point_distance = 0.075f,  max_bad_point_ratio = 0.3f, max_projector_distance = 2.0f, leaf_size = 0.1f, straightness_min_log_ratio = 0.005f;
  float convergence_error = 1e-5, pose_ratio_threshold = 0.3f, damping = 100.0f, max_point_distance = 1.0f;
  std::string input_log = "", output_log = "";
  
  int c = 1;
  while(c < argc) {
    if(!strcmp(argv[c], "-h")) {
      system_utils::printBanner(banner);
      return 0;
    }
    else if(!strcmp(argv[c], "-adaptive_morphing")) { adaptive_morphing = true; }
    else if(!strcmp(argv[c], "-max_neighbors")) { 
      c++;
      max_neighbors = atoi(argv[c]); 
    }
    else if(!strcmp(argv[c], "-refining_steps")) { 
      c++;
      refining_steps = atoi(argv[c]); 
    }
    else if(!strcmp(argv[c], "-trajectory_iterations")) { 
      c++;
      trajectory_iterations = atoi(argv[c]); 
    }
    else if(!strcmp(argv[c], "-local_map_iterations")) { 
      c++;
      local_map_iterations = atoi(argv[c]); 
    }
    else if(!strcmp(argv[c], "-max_normal_angle")) { 
      c++;
      max_normal_angle = atof(argv[c]); 
    }
    else if(!strcmp(argv[c], "-max_point_displacement")) { 
      c++;
      max_point_displacement = atof(argv[c]); 
    }
    else if(!strcmp(argv[c], "-max_energy")) { 
      c++;
      max_energy = atof(argv[c]); 
    }
    else if(!strcmp(argv[c], "-max_energy_candidate_ratio")) { 
      c++;
      max_energy_candidate_ratio = atof(argv[c]); 
    }
    else if(!strcmp(argv[c], "-max_distance")) { 
      c++;
      max_distance = atof(argv[c]); 
    }
    else if(!strcmp(argv[c], "-max_bad_point_distance")) { 
      c++;
      max_bad_point_distance = atof(argv[c]); 
    }
    else if(!strcmp(argv[c], "-max_bad_point_ratio")) { 
      c++;
      max_bad_point_ratio = atof(argv[c]); 
    }
    else if(!strcmp(argv[c], "-max_projector_distance")) { 
      c++;
      max_projector_distance = atof(argv[c]); 
    }
    else if(!strcmp(argv[c], "-convergence_error")) { 
      c++;
      convergence_error = atof(argv[c]); 
    }
    else if(!strcmp(argv[c], "-pose_ratio_threshold")) { 
      c++;
      pose_ratio_threshold = atof(argv[c]); 
    }
    else if(!strcmp(argv[c], "-damping")) { 
      c++;
      damping = atof(argv[c]); 
    }
    else if(!strcmp(argv[c], "-max_point_distance")) { 
      c++;
      max_point_distance = atof(argv[c]); 
    }
    else if(!strcmp(argv[c], "-leaf_size")) { 
      c++;
      leaf_size = atof(argv[c]); 
    }
    else if(!strcmp(argv[c], "-straightness_min_log_ratio")) { 
      c++;
      straightness_min_log_ratio = atof(argv[c]); 
    }
    else if(!strcmp(argv[c], "-o")) { 
      c++;
      output_log = std::string(argv[c]);
    }
    else { input_log = std::string(argv[c]); }
    c++;
  }

  std::list<Serializable*> objects;
  Deserializer des;
  des.setFilePath(input_log);
  Serializable* o;  
  std::list<Serializable*> serializable_objects;
  // Get last id
  int id = -1;
  while((o = des.readObject())) {
    Identifiable* identifiable = dynamic_cast<Identifiable*>(o); 
    if(identifiable && id < identifiable->getId()) { id = identifiable->getId(); }      
    objects.push_back(o);	
  }
  id++;

  NNAligner aligner;
  aligner.finder().setPointsDistance(max_point_displacement);
  aligner.finder().setNormalAngle(max_normal_angle);
  aligner.setIterations(local_map_iterations);
  std::cout << "[INFO]: aligner max point distance " << aligner.finder().pointsDistance() << std::endl;
  std::cout << "[INFO]: aligner max point normal angle " << aligner.finder().normalAngle() << std::endl;
  std::cout << "[INFO]: aligner iterations " << aligner.iterations() << std::endl;
  TrajectoryAligner trajectory_aligner;
  trajectory_aligner.setIterations(trajectory_iterations);
  trajectory_aligner.setConvergenceError(convergence_error);
  trajectory_aligner.correspondeceFinder().setMaxPointDistance(max_point_distance);
  trajectory_aligner.solver().setDamping(damping);
  std::cout << "[INFO]: trajectory aligner max point distance " << trajectory_aligner.correspondeceFinder().maxPointDistance() << std::endl;
  std::cout << "[INFO]: trajectory aligner convergence error " << trajectory_aligner.convergenceError() << std::endl;
  std::cout << "[INFO]: trajectory aligner damping " << trajectory_aligner.solver().damping() << std::endl;
  std::cout << "[INFO]: trajectory aligner iterations " <<  trajectory_aligner.iterations() << std::endl;
  TrajectoryEnergyMorphingCalculator trajectory_energy_calculator;
  trajectory_energy_calculator.setRigidMorphing(!adaptive_morphing);
  trajectory_energy_calculator.setPoseRatioThreshold(pose_ratio_threshold);  
  if(trajectory_energy_calculator.rigidMorphing()) {
    std::cout << "[INFO]: trajectory energy calculator adaptive morphing disabled" << std::endl;
  }
  else {
    std::cout << "[INFO]: trajectory energy calculator adaptive morphing enabled" << std::endl;
  }
  std::cout << "[INFO]: trajectory energy calculator pose ratio threshold " << trajectory_energy_calculator.poseRatioThreshold() << std::endl;
  TrajectoryLoopCloser trajectory_loop_closer(&trajectory_aligner, &trajectory_energy_calculator, &aligner);
  trajectory_loop_closer.setVerbose(true);
  trajectory_loop_closer.setLeafSize(leaf_size);
  trajectory_loop_closer.setMaxNeighbors(max_neighbors);
  trajectory_loop_closer.setTrajecotryAlignerRefiningSteps(refining_steps);
  trajectory_loop_closer.setMaxEnergy(max_energy);
  trajectory_loop_closer.setMaxEnergyCandidateRatio(max_energy_candidate_ratio);
  trajectory_loop_closer.setMaxDistance(max_distance);
  trajectory_loop_closer.setMaxBadPointDistance(max_bad_point_distance);
  trajectory_loop_closer.setMaxBadPointRatio(max_bad_point_ratio);
  trajectory_loop_closer.setMaxProjectorDistance(max_projector_distance);
  trajectory_loop_closer.setStraightnessMinLogRatio(straightness_min_log_ratio);
  std::cout << "[INFO]: trajectory loop closer leaf size " << trajectory_loop_closer.leafSize() << std::endl;
  std::cout << "[INFO]: trajectory loop closer max neighbors " << trajectory_loop_closer.maxNeighbors() << std::endl;
  std::cout << "[INFO]: trajectory loop closer refining steps " << trajectory_loop_closer.trajecotryAlignerRefiningSteps() << std::endl;
  std::cout << "[INFO]: trajectory loop closer max energy " << trajectory_loop_closer.maxEnergy() << std::endl;
  std::cout << "[INFO]: trajectory loop closer max energy candidate ratio " << trajectory_loop_closer.maxEnergyCandidateRatio() << std::endl;
  std::cout << "[INFO]: trajectory loop closer max distance " << trajectory_loop_closer.maxDistance() << std::endl;
  std::cout << "[INFO]: trajectory loop closer max bad point distance " << trajectory_loop_closer.maxBadPointDistance() << std::endl;
  std::cout << "[INFO]: trajectory loop closer max bad point ratio " << trajectory_loop_closer.maxBadPointRatio() << std::endl;
  std::cout << "[INFO]: trajectory loop closer max projector distance " << trajectory_loop_closer.maxProjectorDistance() << std::endl;
  std::cout << "[INFO]: trajectory loop closer straightness min log ratio " << trajectory_loop_closer.straightnessMinLogRatio() << std::endl;

  if(output_log != "") { std::cout << "[INFO]: output saving log " << output_log << std::endl; }

  QApplication app(argc, argv);
  TrajectoryLoopCloserViewer viewer(&trajectory_loop_closer);
  
  std::list<Serializable*>::iterator obj_it = objects.begin();
  LocalMap* new_local_map = 0;
  BinaryNodeRelation* new_relation = 0;
  viewer.init();
  viewer.show();
  while(viewer.isVisible()) {
    app.processEvents();
    
    if(viewer.processNext() || viewer.processAutomatically()) {
      bool found_lmap_and_rel = false;
      while(obj_it != objects.end() && !found_lmap_and_rel) {
	o = *obj_it;
	serializable_objects.push_back(o);
	LocalMap* lmap = dynamic_cast<LocalMap*>(o);
	BinaryNodeRelation* rel = dynamic_cast<BinaryNodeRelation*>(o);
	if(lmap) { 
	  viewer.nodes.addElement(lmap); 
	  new_local_map = lmap;
	}
	if(rel) { 
	  viewer.relations.insert(std::tr1::shared_ptr<BinaryNodeRelation>(rel)); 
	  new_relation = rel;
	}
	if(new_local_map && new_relation && new_relation->to() == new_local_map) { 	  
	  found_lmap_and_rel = true; 
	}
	obj_it++;
      }      
      viewer.setLastLocalMap(new_local_map);
      BinaryNodeRelationSet& accepted_closures = viewer.optimizeMap();
      for(BinaryNodeRelationSet::iterator it = accepted_closures.begin(); 
      	  it != accepted_closures.end();
      	  ++it) {
      	BinaryNodeRelation* closure = it->get();
      	closure->setId(id);
      	id++;
      	serializable_objects.push_back(closure);
      }

      viewer.updateGL();
      double t_start = getTime();
      trajectory_loop_closer.findClosures(new_local_map, &viewer.nodes);
      double t_end = getTime();
      std::cout << std::setprecision(5) << std::fixed <<"[INFO]: loop closing time " << t_end - t_start << " s" << std::endl;
      
      found_lmap_and_rel = false;
      new_local_map = 0;
      new_relation = 0;
      viewer.setProcessNext(false);
      viewer.setNeedRedraw(true);
    }

    if(output_log != "" && viewer.saveMap()) {
      std::cout << "[INFO]: saving log to " << output_log << std::endl;
      Serializer ser;
      ser.setFilePath(output_log);
      ser.setBinaryPath(output_log + ".d/<classname>.<nameAttribute>.<id>.<ext>");
      for(std::list<Serializable*>::iterator it = serializable_objects.begin(); 
	  it != serializable_objects.end(); 
	  ++it) {
	Serializable* s = *it;
	ser.writeObject(*s);
      }
      viewer.setSaveMap(false);
    }
    
    if(viewer.needRedraw()) { viewer.updateGL(); }
    else { usleep(10000); }
  }

  if(output_log != "") {
    std::cout << "[INFO]: saving log to " << output_log << std::endl;
    Serializer ser;
    ser.setFilePath(output_log);
    ser.setBinaryPath(output_log + ".d/<classname>.<nameAttribute>.<id>.<ext>");
    for(std::list<Serializable*>::iterator it = serializable_objects.begin(); 
	it != serializable_objects.end(); 
	++it) {
      Serializable* s = *it;
      ser.writeObject(*s);
    }
  }
  
  return 0;
}
