#include "trajectory_loop_closer.h"

#include "fps_core/depth_utils.h"
#include "fps_core/nn_aligner.h"
#include "fps_core/pinhole_camera_info.h"
#include "fps_map/multi_image_map_node.h"
#include "fps_map/image_map_node.h"

#include <opencv2/opencv.hpp>

namespace fps_mapper {
  
  struct ClosureCandidate {
    ClosureCandidate(std::tr1::shared_ptr<MapNode> map_node_, Eigen::Isometry3f initial_guess_, 
		     bool energy_based_) {
      energy_based = energy_based_;
      map_node = map_node_;
      initial_guess = initial_guess_;
    }
    
    bool energy_based;
    std::tr1::shared_ptr<MapNode> map_node;
    Eigen::Isometry3f initial_guess;
  };

  TrajectoryLoopCloser::TrajectoryLoopCloser(TrajectoryAligner* trajectory_aligner_, 
					     TrajectoryEnergyCalculator* trajecotry_energy_calculator_, 
					     BaseAligner* aligner_): BaseLoopCloser(aligner_) {
    _max_neighbors = 5;
    _trajectory_aligner_refining_steps = 3;
    _max_energy = 0.06f;
    _max_energy_candidate_ratio = 0.15f;
    _straightness_min_log_ratio = 0.3f;
    _max_distance = 3.0f;
    _max_bad_point_distance = 0.075f;
    _max_bad_point_ratio = 0.1f;
    _max_projector_distance = 2.0f;
    _projector = 0;
    _leaf_size = 0.1f;
    setTrajectoryAligner(trajectory_aligner_);
    setTrajectoryEnergyCalculator(trajecotry_energy_calculator_);
    _in_distance = 0.0f;
    _out_distance = 0.0f;
    _in_num = 0;
    _out_num = 0;
  }

  TrajectoryLoopCloser::~TrajectoryLoopCloser() { 
    if(_projector) { delete _projector; }
    _projector = 0;
  }

  void TrajectoryLoopCloser::findClosures(LocalMap* target_local_map, MapNodeList* source_local_maps) {
    // clear structures
    _target_node = target_local_map;
    _candidate_closure_nodes.clear();
    _accepted_closure_nodes.clear();
    _accepted_closure_relations.clear();

    // find distance and energy based candidate nodes
    if(_verbose) { std::cout << "[INFO]: <--- Seraching for closures --->" << std::endl; }   
    if(!target_local_map) { return; }
    _target_node = target_local_map;
    MapNodeSet neighbor_closure_candidates_nodes, energy_closure_candidates_nodes;
    std::map<float, ClosureCandidate> closure_candidates, neighbor_closure_candidates, energy_closure_candidates;
    bool target_is_straight = is_straight_trajectory(target_local_map);
    if(_verbose) { 
      if(target_is_straight) { std::cout << "[INFO]: current local map has a straight trajectory" << std::endl; }
      else { std::cout << "[INFO]: current local map has a curved trajectory" << std::endl; }
    }
    for(MapNodeList::const_iterator it = source_local_maps->begin(); it !=  source_local_maps->end(); ++it) {
      LocalMap* source_local_map = dynamic_cast<LocalMap*>(it->get());
      if(!source_local_map) { continue; }
      if(source_local_map == target_local_map) { continue; }      
      bool source_is_straight = is_straight_trajectory(source_local_map);
      Eigen::Isometry3f initial_guess = target_local_map->transform().inverse() * source_local_map->transform(); 
      float distance = initial_guess.translation().norm();      
      // neighbor based candidate
      if(distance < _max_distance && 	 
	 abs(target_local_map->getId() - source_local_map->getId()) > 150 && 
	 target_local_map->nodes().size() > 10 && source_local_map->nodes().size() > 10) {
      	ClosureCandidate closure_candidate(*it, initial_guess, false);
      	neighbor_closure_candidates.insert(std::pair<float, ClosureCandidate>(distance, closure_candidate));	
      }
      // energy based candidate
      else if(target_local_map->nodes().size() > 20 && source_local_map->nodes().size() > 20 && 
	      !target_is_straight && !source_is_straight) {
	initial_guess = align_trajectories(target_local_map, source_local_map); 
	float energy = _trajectory_energy_calculator->compute(target_local_map, 
							      source_local_map, 
							      initial_guess);
	if(energy > 0 && energy < _max_energy) {
	  ClosureCandidate closure_candidate(*it, initial_guess, true);
	  energy_closure_candidates.insert(std::pair<float, ClosureCandidate>(distance, closure_candidate));
	  energy_closure_candidates_nodes.insert(it->get());	  
	}
      }
    }
    // Keep only a maximum number of neighbor nodes
    int i;
    std::map<float, ClosureCandidate>::iterator it;
    for(it = neighbor_closure_candidates.begin(), i = 0; it != neighbor_closure_candidates.end() && i < _max_neighbors; ++it, ++i) {
      closure_candidates.insert(*it);
      _candidate_closure_nodes.insert(it->second.map_node.get());
    }
    if(_verbose) { 
      std::cout << "[INFO]: neighbor candidates " << neighbor_closure_candidates.size() << std::endl; 
      std::cout << "[INFO]: energy   candidates " << energy_closure_candidates.size() << std::endl; 
      std::cout << "[INFO]: energy based candidate ratio " << (float)energy_closure_candidates_nodes.size() / (float)source_local_maps->size() << std::endl; 
    }
    // Discard energy based candidates if they are too much
    if((float)energy_closure_candidates_nodes.size() / (float)source_local_maps->size() < _max_energy_candidate_ratio) {
      closure_candidates.insert(energy_closure_candidates.begin(), energy_closure_candidates.end());
      _candidate_closure_nodes.insert(energy_closure_candidates_nodes.begin(), energy_closure_candidates_nodes.end());
    }
    if(_verbose) { std::cout << "[INFO]: found " << _candidate_closure_nodes.size() << " candidate closure nodes" << std::endl; }

    // align candidates and perform geometry check on each of them
    for(std::map<float, ClosureCandidate>::iterator candidate_it = closure_candidates.begin(); 
	candidate_it != closure_candidates.end();
	++candidate_it) {
      ClosureCandidate& closure_candidate = candidate_it->second; 
      Eigen::Isometry3f initial_guess = closure_candidate.initial_guess; 
      std::tr1::shared_ptr<MapNode>& source_node = closure_candidate.map_node;
      LocalMap* source_local_map = dynamic_cast<LocalMap*>(source_node.get());
      if(source_local_map) { 
      	BinaryNodeRelation* accepted_closure_relation = 0;
      	alignAndVerifyConsistency(accepted_closure_relation,
				  target_local_map, source_local_map, 
				  initial_guess,
				  closure_candidate.energy_based);
      	if(accepted_closure_relation) { 
      	  _accepted_closure_nodes.insert(source_node.get());
      	  _accepted_closure_relations.insert(std::tr1::shared_ptr<BinaryNodeRelation>(accepted_closure_relation));
      	}
      }
    }
    if(_verbose) { std::cout << "[INFO]: accepted " << _accepted_closure_nodes.size() << " closure nodes " << std::endl; }
  }

  Eigen::Isometry3f TrajectoryLoopCloser::align_trajectories(LocalMap* target_local_map, LocalMap* source_local_map) const {
    Eigen::Isometry3f registration_transfrom = Eigen::Isometry3f::Identity();
    _trajectory_aligner->setTargetLocalMap(target_local_map);
    _trajectory_aligner->setSourceLocalMap(source_local_map);

    for(int i = 0; i < _trajectory_aligner_refining_steps; ++i) {
      Eigen::Isometry3f initial_guess = Eigen::Isometry3f::Identity() * registration_transfrom;
      _trajectory_aligner->align(initial_guess);    
      float error_without_initial_guess = _trajectory_aligner->solver().error();
      registration_transfrom = _trajectory_aligner->transform();

      initial_guess.linear() = Eigen::AngleAxisf(M_PI, Eigen::Vector3f::UnitZ()).toRotationMatrix();;
      initial_guess.translation() = Eigen::Vector3f::Zero();
      _trajectory_aligner->align(initial_guess);    
      float error_with_initial_guess = _trajectory_aligner->solver().error();    
    
      if(error_with_initial_guess < error_without_initial_guess) { registration_transfrom = _trajectory_aligner->transform(); }
    }
    
    return registration_transfrom;
  }

  void TrajectoryLoopCloser::setProjector(const LocalMap* local_map) {
    for(MapNodeList::const_iterator it = local_map->nodes().begin(); it != local_map->nodes().end(); ++it) {            
      std::tr1::shared_ptr<MapNode> node = *it;
      if(!_projector && local_map) {
	MultiImageMapNode* mim_node = dynamic_cast<MultiImageMapNode*>(it->get());
	if(mim_node) {
	  MultiProjector *_projector = new MultiProjector(); 
	  _projector->initFromCameraInfo(mim_node->cameraInfo());

	  // determine total image size of the multi projector
	  int rows = 0, cols = 0;	  
	  for(size_t i = 0; i < _projector->projectors().size(); ++i) {
	    PinholeProjector* projector = dynamic_cast<PinholeProjector*>(_projector->projectors()[i]);
	    if(!projector) { throw std::runtime_error("[ERROR]: TrajectoryLoopCloser -> multi point projector contains projectors different from pinhole"); }
	    if(cols == 0) { cols = projector->K()(0, 2); }
	    else if(cols != projector->K()(0, 2)) { throw std::runtime_error("[ERROR]: TrajectoryLoopCloser -> multi point projector contains projectors with different dimensions"); } 
	    rows += projector->K()(1, 2);
	  }
	  	  
	  _projector->setImageSize(rows, cols);
	  _projector->setMaxDistance(_max_projector_distance);
	    this->_projector=_projector;
	} else {
	  ImageMapNode* pim_node = dynamic_cast<ImageMapNode*>(it->get());
	  if(pim_node) {
	    PinholeProjector* _projector = new PinholeProjector(); 
	    _projector->setCameraInfo(pim_node->cameraInfo());

	    // determine total image size of the multi projector
	    int rows = _projector->K()(1, 2)*2, cols = _projector->K()(0, 2)*2;	  
	    _projector->setImageSize(rows, cols);
	    _projector->setMaxDistance(_max_projector_distance);
	    this->_projector=_projector;
	  } else { 
	    throw std::runtime_error("[ERROR]: TrajectoryLoopCloser -> local map trajectory poses are expected to be MultiImageNode or a pinhole image map node"); 
	  }
	}
      }     
    }
  }
  
  /* Aligns and performs geometry check on two input local maps */
  void TrajectoryLoopCloser::alignAndVerifyConsistency(BinaryNodeRelation*& closure_relation,
						       LocalMap* target_local_map, LocalMap* source_local_map, 
						       Eigen::Isometry3f initial_guess, bool energy_based) {    
    _in_distance = _out_distance = 0;
    _in_num = _out_num = 0;
    closure_relation = matchLocalMaps(target_local_map, source_local_map, initial_guess, _leaf_size);
    for(MapNodeList::iterator it = target_local_map->nodes().begin(); 
    	it != target_local_map->nodes().end(); 
    	++it) {
      std::tr1::shared_ptr<MapNode> target_node = *it;
      if(!_projector) { setProjector(target_local_map); }
      _target_depth.setTo(cv::Scalar(0));
      _source_depth.setTo(cv::Scalar(0));
      _projector->project(_target_depth, _target_indices, target_node->transform().inverse(), *target_local_map->cloud());        
      Eigen::Isometry3f offset = (closure_relation->transform().inverse() * target_node->transform()).inverse();
      _projector->project(_source_depth, _source_indices, offset, *source_local_map->cloud());
      computeConsistency(_target_depth, _target_indices, _source_depth, _source_indices);     
    }
    float ratio = (float)_out_num / (float)(_in_num + _out_num); 
    if(_verbose) { 
      std::cout << "[INFO]: outliers " << _out_num << std::endl;
      std::cout << "[INFO]: inliers " << _in_num << std::endl;
      std::cout << "[INFO]: outliers ratio " << ratio << std::endl;
    }
    bool good_closure = true;
    if(ratio != ratio) { good_closure = false; }  
    float adapted_ratio = _max_bad_point_ratio;
    if(energy_based) { adapted_ratio = _max_bad_point_ratio / 4.0f; }
    if(ratio > adapted_ratio) { good_closure = false; }
    if(!good_closure) {
      delete closure_relation;
      closure_relation = 0;
    }    
  }

  void TrajectoryLoopCloser::computeConsistency(const FloatImage& target_depth, const IntImage& target_indices,
						const FloatImage& source_depth, const IntImage& source_indices) {
    float in_distance, out_distance;
    int in_num, out_num;
    compareDepths(in_distance, in_num, out_distance, out_num, 
		  target_depth, target_indices, source_depth, source_indices, 
		  _max_bad_point_distance, false);
    _in_distance += in_distance;
    _in_num += in_num; 
    _out_distance += out_distance;      
    _out_num += out_num;
  }

  bool TrajectoryLoopCloser::is_straight_trajectory(LocalMap* local_map) {
    float x = 0.0f, xx = 0.0f;
    float y = 0.0f, yy = 0.0f;
    float z = 0.0f, zz = 0.0f;
    float vx = 0.0f, vy = 0.0f, vz = 0.0f;    
    for(MapNodeList::iterator it = local_map->nodes().begin(); it != local_map->nodes().end(); ++it) {
      x  += (*it)->transform().translation().x();
      xx += (*it)->transform().translation().x() * (*it)->transform().translation().x(); 
      y  += (*it)->transform().translation().y();
      yy += (*it)->transform().translation().y() * (*it)->transform().translation().y(); 
      z  += (*it)->transform().translation().z();
      zz += (*it)->transform().translation().z() * (*it)->transform().translation().z(); 
    }      
    float n_inv = 1.0f / (float)local_map->nodes().size();    
    vx = n_inv * (xx - n_inv * x * x); 
    vy = n_inv * (yy - n_inv * y * y); 
    vz = n_inv * (zz - n_inv * z * z); 

    if(vx > _straightness_min_log_ratio && vy > _straightness_min_log_ratio) { return false; }
    
    return true;
  }

}
