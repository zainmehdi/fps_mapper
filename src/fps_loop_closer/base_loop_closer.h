#pragma once

#include "fps_core/base_aligner.h"
#include "fps_map/local_map.h"

namespace fps_mapper {

  /* The BaseLoopCloser class is an interface where to inherit the base methods for a loop closer algorithm */
  class BaseLoopCloser {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    /* Constructors and destructor */
    BaseLoopCloser(BaseAligner* aligner_) { 
      _target_node = 0;
      setAligner(aligner_); 
    }
    virtual ~BaseLoopCloser() {}
  
    /* Get methods */
    inline bool verbose() const { return _verbose; }
    inline float leafSize() const { return _leaf_size; }
    inline MapNode* targetNode() { return _target_node; }
    inline MapNodeSet& candidateClosureNodes() { return _candidate_closure_nodes; }
    inline MapNodeSet& acceptedClosureNodes() { return _accepted_closure_nodes; }
    inline BinaryNodeRelationSet& acceptedClosureRelations() { return _accepted_closure_relations; }
    inline BaseAligner* aligner() { return _aligner; }

    /* Set methods */
    inline void setVerbose(const float verbose_) { _verbose = verbose_; }
    inline void setLeafSize(const float leaf_size_) { _leaf_size = leaf_size_; }
    inline void setAligner(BaseAligner* aligner_) { _aligner = aligner_; }

    /* Find closures for the input local map */
    virtual void findClosures(LocalMap* target_local_map, MapNodeList* source_local_maps) = 0;

    /* Register two local maps */
    virtual BinaryNodeRelation* matchLocalMaps(LocalMap* target_local_map, LocalMap* source_local_map, 
					       Eigen::Isometry3f initial_guess = Eigen::Isometry3f::Identity(), 
					       float voxel_leaf = 0.05f);
      
  protected:
    bool _verbose;
    float _leaf_size;
    /* Last node on which closures where searched on */
    MapNode* _target_node;
    /* Nodes selected as candidate closures */
    MapNodeSet _candidate_closure_nodes;
    /* Nodes accepted as candidate closures */
    MapNodeSet _accepted_closure_nodes;
    /* New relations for the accepted closures */
    BinaryNodeRelationSet _accepted_closure_relations;
    /* Aligner for registration of pairs of local maps */
    BaseAligner* _aligner;
  };

}
