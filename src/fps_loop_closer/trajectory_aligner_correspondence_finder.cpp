#include "trajectory_aligner_correspondence_finder.h"
#include "trajectory_aligner_solver.h"

namespace fps_mapper {

  TrajectoryAlignerCorrespondenceFinder::TrajectoryAlignerCorrespondenceFinder(TrajectoryAlignerSolver* solver_) {
    _max_point_distance = 1.0f;
    _solver = solver_;    
  }

  void TrajectoryAlignerCorrespondenceFinder::findCorrespondences() {
    _correspondences.clear();
    Eigen::Isometry3f transform = _solver->transform();
    const LocalMap* target_local_map = _solver->targetLocalMap();
    const LocalMap* source_local_map = _solver->sourceLocalMap();

    if(!target_local_map) {  
      throw std::runtime_error("[ERROR]: TrajectoryAlignerCorrespondenceFinder's solver has target local map not set"); 
    }
    if(!source_local_map) {
      throw std::runtime_error("[ERROR]: TrajectoryAlignerCorrespondenceFinder's solver has source local map not set");
    }

    const MapNodeList& target_trajectory = target_local_map->nodes();
    const MapNodeList& source_trajectory = source_local_map->nodes();
    for(MapNodeList::const_iterator source_trajectory_it = source_trajectory.begin(); 
	source_trajectory_it != source_trajectory.end(); 
	++source_trajectory_it) {
      std::tr1::shared_ptr<MapNode> source_trajectory_node = *source_trajectory_it;
      Eigen::Vector3f source_trajectory_point = transform * source_trajectory_node->transform().translation();
      float min_distance = std::numeric_limits<float>::max();
      TrajectoryAlignerCorrespondenceFinder::Correspondence correspondence;
      correspondence.second = source_trajectory_node;
      for(MapNodeList::const_iterator target_trajectory_it = target_trajectory.begin(); 
	  target_trajectory_it != target_trajectory.end(); 
	  ++target_trajectory_it) {
	std::tr1::shared_ptr<MapNode> target_trajectory_node = *target_trajectory_it;
	Eigen::Vector3f target_trajectory_point = target_trajectory_node->transform().translation();
	float distance = (target_trajectory_point - source_trajectory_point).norm();
	if(distance < min_distance) { 
	  min_distance = distance; 
	  correspondence.first = target_trajectory_node;
	}
      }
      if(min_distance < _max_point_distance) { _correspondences.push_back(correspondence); }
    }
  }

}
