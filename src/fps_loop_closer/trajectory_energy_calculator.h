#pragma once

#include "fps_map/local_map.h"

namespace fps_mapper {

  /* The TrajectoryEnergyCalculator class is an interface for any class whose goal is to
     compute a sort of energy between two local map trajectories */
  class TrajectoryEnergyCalculator {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

    /* Compute  energy */
    virtual float compute(const LocalMap* target_local_map, const LocalMap* source_local_map, 
			  Eigen::Isometry3f source_offset = Eigen::Isometry3f::Identity()) const = 0;  
  };
    
}
