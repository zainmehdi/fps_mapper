#include "trajectory_energy_morphing_calculator.h"

namespace fps_mapper {

  TrajectoryEnergyMorphingCalculator::TrajectoryEnergyMorphingCalculator() {
    _rigid_morphig = false;
    _pose_ratio_threshold = 0.85f;    
  }

  float TrajectoryEnergyMorphingCalculator::compute(const LocalMap* target_local_map, 
						    const LocalMap* source_local_map, 
						    Eigen::Isometry3f source_offset) const {
    float energy = 0;
    Eigen::Isometry3f update = Eigen::Isometry3f::Identity();
    const MapNodeList* source_nodes = &source_local_map->nodes();
    const MapNodeList* target_nodes = &target_local_map->nodes();
    if(source_nodes->size() > target_nodes->size()) {
      source_nodes = &target_local_map->nodes();
      target_nodes = &source_local_map->nodes();      
      source_offset = source_offset.inverse();
    }

    if(fabs((float)source_nodes->size() / (float)target_nodes->size() - 1.0f) > _pose_ratio_threshold) { return -1; }

    std::set<const MapNode*> nearest_nodes;
    for(MapNodeList::const_iterator source_nodes_it = source_nodes->begin(); 
	source_nodes_it != source_nodes->end(); 
	++source_nodes_it) {       
      float distance;
      Eigen::Isometry3f delta_T; 
      const std::tr1::shared_ptr<MapNode>& source_node = *source_nodes_it;
      _find_nearest_node(nearest_nodes,
			 distance, delta_T, 
			 *target_nodes, source_node, 
			 source_offset, update);      
      if(_rigid_morphig) {
	update = update * delta_T;
	Eigen::Matrix3f R = update.linear();
	Eigen::Matrix3f E = R.transpose() * R;
	E.diagonal().array() -= 1;
	update.linear() -= 0.5 * R * E;
      }

      energy += distance;
    }

    return energy / (float)nearest_nodes.size();
  }

  void TrajectoryEnergyMorphingCalculator::_find_nearest_node(std::set<const MapNode*>& nearest_nodes,
							      float& min_distance, Eigen::Isometry3f& delta_transform, 
							      const MapNodeList& target_nodes, 
							      const std::tr1::shared_ptr<MapNode>& source_node, 
							      const Eigen::Isometry3f source_offset, 
							      Eigen::Isometry3f update) const {
    if(target_nodes.size() == 0) {
      throw std::runtime_error("[ERROR]: input target_poses list in _find_nearest_pose function is empty");
    }
        
    min_distance = std::numeric_limits<float>::max();    
    delta_transform.setIdentity();
    MapNodeList::const_iterator nearest_node_it = target_nodes.end();
    for(MapNodeList::const_iterator target_nodes_it = target_nodes.begin(); 
	target_nodes_it != target_nodes.end(); 
	++target_nodes_it) { 
      const std::tr1::shared_ptr<MapNode>& target_node = *target_nodes_it;
      Eigen::Isometry3f delta_T = update * (source_offset * source_node->transform()).inverse() * target_node->transform();
      float distance = delta_T.translation().norm();
      if(distance < min_distance && nearest_nodes.find(target_nodes_it->get()) == nearest_nodes.end()) {
	nearest_node_it = target_nodes_it;
	min_distance = distance;
	delta_transform = delta_T;
      }
    }
    if(nearest_node_it != target_nodes.end()) { nearest_nodes.insert(nearest_node_it->get()); }
    else { min_distance = 0.0f; }
  }

}
