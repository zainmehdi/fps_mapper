#pragma once

#include "fps_map/map_node.h"

namespace fps_mapper {

  class TrajectoryAlignerSolver;

  /* The TrajectoryAlignerCorrespondenceFinder search for corresponding points between 
     two local map trajectories stored in the solver*/
  class TrajectoryAlignerCorrespondenceFinder {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

    /* A correspondence is a pair of MapNode objects */
    typedef std::pair<std::tr1::shared_ptr<MapNode>, std::tr1::shared_ptr<MapNode> > Correspondence;
    /* Correspondence vector*/
    typedef std::vector<Correspondence> CorrespondenceVector;
    
    /* Constructors and destructor */
    TrajectoryAlignerCorrespondenceFinder(TrajectoryAlignerSolver* solver_);
    virtual ~TrajectoryAlignerCorrespondenceFinder() {}

    /* Get methods */
    inline float maxPointDistance() const { return _max_point_distance; }
    inline const CorrespondenceVector& correspondences() const { return _correspondences; }
    inline TrajectoryAlignerSolver& solver() { return *_solver; }

    /* Set methods */
    inline void setMaxPointDistance(const float max_point_distance_) { _max_point_distance = max_point_distance_; }
    inline void setSolver(TrajectoryAlignerSolver* solver_) { _solver = solver_; }

    /* Search for correspondences */
    virtual void findCorrespondences();

  protected:
    /* Max allowed distance between two points to be considered a correspondence  */
    float _max_point_distance;
    /* Vector of correspondences */
    CorrespondenceVector _correspondences;
    /* Linearizer storing the local maps trajectories where to search for correspondences */
    TrajectoryAlignerSolver* _solver;

  };

}
