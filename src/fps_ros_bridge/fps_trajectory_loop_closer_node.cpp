#include <sys/time.h>
#include <stdexcept>
#include <qapplication.h>
#include <qevent.h>

#include <tf/transform_broadcaster.h>

#include "boss/deserializer.h"
#include "boss/serializer.h"
#include "fps_core/nn_aligner.h"
#include "fps_mapper/MapUpdateMsg.h"
#include "fps_ros_bridge/local_map_viewer.h"
#include "globals/system_utils.h"
#include "fps_global_optimization/g2o_bridge.h"

#include "fps_loop_closer/trajectory_loop_closer.h"
#include "fps_loop_closer/trajectory_energy_morphing_calculator.h"
#include "trajectory_loop_closer_node_viewer.h"

using namespace fps_mapper;
using namespace boss;
using namespace system_utils;

const char* banner[] = {
  "fps_trajectory_loop_closer: perform loop closure on an input log",
  "usage:",
  " fps_trajectory_loop_closer_node [options]",  
  " where: ",
  " -h                                  [], prints this help",
  " -adaptive_morphing              [bool], apply adaptive morphing (1) when computing trajectory energy, default [true]",
  " -enable_viewer                  [bool], enable (1) or disable (0) viewer, default [true]",
  " -verbose                        [bool], enable (1) or disable (0) verbose mode, default [false]",
  " -max_neighbors                   [int], maximum number of neighbors to consider for the loop closure, default [5]",
  " -refining_steps                  [int], number of refining alignments when registering two trajectories, default [1]",
  " -trajectory_iterations           [int], trajectories aligning iterations, default [10]",
  " -local_map_iterations            [int], local maps aligning iterations, default [20]",
  " -max_normal_angle              [float], max normal angle between two corresponding points in the local map alignment, default [PI/4]",
  " -max_point_displacement        [float], max distance between two corresponding points in the local map alignment, default [1.5]",
  " -max_energy                    [float], max energy for a local map trajectory to be considered a candidate for a closure, default [0.3]",
  " -max_energy_candidate_ratio    [float], maximum ratio of energy based candidates for the current local map, default [0.1]",
  " -max_distance                  [float], maximum distance radius where to search for neighbors candidates, default [3]",
  " -max_bad_point_distance        [float], maximum point distance for a point to be considered an inlier in the geometry check, default [0.075]",
  " -max_bad_point_ratio           [float], maximum bad point ratio for the geometry check, default [0.4]",
  " -max_projector_distance        [float], maximum projector distance used in the geometry check, default [2]",
  " -convergence_error             [float], convergence error threshold for trajectory alignment, default [0.00001]",
  " -pose_ratio_threshold          [float], pose ratio threshold between two local maps, default [0.3]",
  " -damping                       [float], damping factor for the trajectory aligner, default [100]",
  " -max_point_distance            [float], max distance for a correspondence in the trajectory alignment, default [1]",
  " -leaf_size                     [float], leaf size of the voxel grid applied to the local maps before registration, default [0.1]",
  " -straightness_min_log_ratio    [float], minimum threshold for which a trajectory is considered curved, default [0.005]",
  " -o                            [string], filename of the log where to save the map",
  "",
  " while running:",
  " C: clear all local maps",
  " V: show all local maps",
  " O: optimize the graph",
  " S: save the current map",
  " F: save screenshots",
  0
};


int main (int argc, char** argv) {
  // Corridor like environment
  bool adaptive_morphing = true, enable_viewer = true, verbose = false;
  int max_neighbors = 5, refining_steps = 1, trajectory_iterations = 10, local_map_iterations = 20;
  float max_normal_angle = M_PI / 4.0f, max_point_displacement = 1.5f, max_energy = 0.3f, max_energy_candidate_ratio = 0.1;
  float max_distance = 3.0f, max_bad_point_distance = 0.075f,  max_bad_point_ratio = 0.4f, max_projector_distance = 2.0f, leaf_size = 0.1f, straightness_min_log_ratio = 0.005f;
  float convergence_error = 1e-5, pose_ratio_threshold = 0.3f, damping = 100.0f, max_point_distance = 1.0f;
  std::string input_log = "", output_log = "";
  
  int c = 1;
  while(c < argc) {
    if(!strcmp(argv[c], "-h")) {
      system_utils::printBanner(banner);
      return 0;
    }
    else if(!strcmp(argv[c], "-adaptive_morphing")) { 
      c++;
      adaptive_morphing = atoi(argv[c]); 
    }
    else if(!strcmp(argv[c], "-verbose")) { 
      c++;
      verbose = atoi(argv[c]); 
    }
    else if(!strcmp(argv[c], "-max_neighbors")) { 
      c++;
      max_neighbors = atoi(argv[c]); 
    }
    else if(!strcmp(argv[c], "-refining_steps")) { 
      c++;
      refining_steps = atoi(argv[c]); 
    }
    else if(!strcmp(argv[c], "-trajectory_iterations")) { 
      c++;
      trajectory_iterations = atoi(argv[c]); 
    }
    else if(!strcmp(argv[c], "-local_map_iterations")) { 
      c++;
      local_map_iterations = atoi(argv[c]); 
    }
    else if(!strcmp(argv[c], "-max_normal_angle")) { 
      c++;
      max_normal_angle = atof(argv[c]); 
    }
    else if(!strcmp(argv[c], "-max_point_displacement")) { 
      c++;
      max_point_displacement = atof(argv[c]); 
    }
    else if(!strcmp(argv[c], "-max_energy")) { 
      c++;
      max_energy = atof(argv[c]); 
    }
    else if(!strcmp(argv[c], "-max_energy_candidate_ratio")) { 
      c++;
      max_energy_candidate_ratio = atof(argv[c]); 
    }
    else if(!strcmp(argv[c], "-max_distance")) { 
      c++;
      max_distance = atof(argv[c]); 
    }
    else if(!strcmp(argv[c], "-max_bad_point_distance")) { 
      c++;
      max_bad_point_distance = atof(argv[c]); 
    }
    else if(!strcmp(argv[c], "-max_bad_point_ratio")) { 
      c++;
      max_bad_point_ratio = atof(argv[c]); 
    }
    else if(!strcmp(argv[c], "-max_projector_distance")) { 
      c++;
      max_projector_distance = atof(argv[c]); 
    }
    else if(!strcmp(argv[c], "-convergence_error")) { 
      c++;
      convergence_error = atof(argv[c]); 
    }
    else if(!strcmp(argv[c], "-pose_ratio_threshold")) { 
      c++;
      pose_ratio_threshold = atof(argv[c]); 
    }
    else if(!strcmp(argv[c], "-damping")) { 
      c++;
      damping = atof(argv[c]); 
    }
    else if(!strcmp(argv[c], "-max_point_distance")) { 
      c++;
      max_point_distance = atof(argv[c]); 
    }
    else if(!strcmp(argv[c], "-leaf_size")) { 
      c++;
      leaf_size = atof(argv[c]); 
    }
    else if(!strcmp(argv[c], "-straightness_min_log_ratio")) { 
      c++;
      straightness_min_log_ratio = atof(argv[c]); 
    }
    else if(!strcmp(argv[c], "-enable_viewer")) {
      c++;
      enable_viewer = atoi(argv[c]);
    }
    else if(!strcmp(argv[c], "-o")) { 
      c++;
      output_log = std::string(argv[c]);
    }
    c++;
  }

  NNAligner aligner;
  aligner.finder().setPointsDistance(max_point_displacement);
  aligner.finder().setNormalAngle(max_normal_angle);
  aligner.setIterations(local_map_iterations);
  std::cout << "[INFO]: aligner max point distance " << aligner.finder().pointsDistance() << std::endl;
  std::cout << "[INFO]: aligner max point normal angle " << aligner.finder().normalAngle() << std::endl;
  std::cout << "[INFO]: aligner iterations " << aligner.iterations() << std::endl;
  TrajectoryAligner trajectory_aligner;
  trajectory_aligner.setIterations(trajectory_iterations);
  trajectory_aligner.setConvergenceError(convergence_error);
  trajectory_aligner.correspondeceFinder().setMaxPointDistance(max_point_distance);
  trajectory_aligner.solver().setDamping(damping);
  std::cout << "[INFO]: trajectory aligner max point distance " << trajectory_aligner.correspondeceFinder().maxPointDistance() << std::endl;
  std::cout << "[INFO]: trajectory aligner convergence error " << trajectory_aligner.convergenceError() << std::endl;
  std::cout << "[INFO]: trajectory aligner damping " << trajectory_aligner.solver().damping() << std::endl;
  std::cout << "[INFO]: trajectory aligner iterations " <<  trajectory_aligner.iterations() << std::endl;
  TrajectoryEnergyMorphingCalculator trajectory_energy_calculator;
  trajectory_energy_calculator.setRigidMorphing(!adaptive_morphing);
  trajectory_energy_calculator.setPoseRatioThreshold(pose_ratio_threshold);  
  if(trajectory_energy_calculator.rigidMorphing()) {
    std::cout << "[INFO]: trajectory energy calculator adaptive morphing disabled" << std::endl;
  }
  else {
    std::cout << "[INFO]: trajectory energy calculator adaptive morphing enabled" << std::endl;
  }
  std::cout << "[INFO]: trajectory energy calculator pose ratio threshold " << trajectory_energy_calculator.poseRatioThreshold() << std::endl;
  TrajectoryLoopCloser trajectory_loop_closer(&trajectory_aligner, &trajectory_energy_calculator, &aligner);
  trajectory_loop_closer.setVerbose(verbose);
  trajectory_loop_closer.setLeafSize(leaf_size);
  trajectory_loop_closer.setMaxNeighbors(max_neighbors);
  trajectory_loop_closer.setTrajecotryAlignerRefiningSteps(refining_steps);
  trajectory_loop_closer.setMaxEnergy(max_energy);
  trajectory_loop_closer.setMaxEnergyCandidateRatio(max_energy_candidate_ratio);
  trajectory_loop_closer.setMaxDistance(max_distance);
  trajectory_loop_closer.setMaxBadPointDistance(max_bad_point_distance);
  trajectory_loop_closer.setMaxBadPointRatio(max_bad_point_ratio);
  trajectory_loop_closer.setMaxProjectorDistance(max_projector_distance);
  trajectory_loop_closer.setStraightnessMinLogRatio(straightness_min_log_ratio);
  std::cout << "[INFO]: trajectory loop closer leaf size " << trajectory_loop_closer.leafSize() << std::endl;
  std::cout << "[INFO]: trajectory loop closer max neighbors " << trajectory_loop_closer.maxNeighbors() << std::endl;
  std::cout << "[INFO]: trajectory loop closer refining steps " << trajectory_loop_closer.trajecotryAlignerRefiningSteps() << std::endl;
  std::cout << "[INFO]: trajectory loop closer max energy " << trajectory_loop_closer.maxEnergy() << std::endl;
  std::cout << "[INFO]: trajectory loop closer max energy candidate ratio " << trajectory_loop_closer.maxEnergyCandidateRatio() << std::endl;
  std::cout << "[INFO]: trajectory loop closer max distance " << trajectory_loop_closer.maxDistance() << std::endl;
  std::cout << "[INFO]: trajectory loop closer max bad point distance " << trajectory_loop_closer.maxBadPointDistance() << std::endl;
  std::cout << "[INFO]: trajectory loop closer max bad point ratio " << trajectory_loop_closer.maxBadPointRatio() << std::endl;
  std::cout << "[INFO]: trajectory loop closer max projector distance " << trajectory_loop_closer.maxProjectorDistance() << std::endl;
  std::cout << "[INFO]: trajectory loop closer straightness min log ratio " << trajectory_loop_closer.straightnessMinLogRatio() << std::endl;

  if(output_log != "") { std::cout << "[INFO]: output saving log " << output_log << std::endl; }
  if(enable_viewer) { std::cout << "[INFO]: viewer enabled" << std::endl; }
  else { std::cout << "[INFO]: viewer disabled " << std::endl; }
  if(verbose) { std::cout << "[INFO]: verbose mode enabled" << std::endl; }
  else { std::cout << "[INFO]: verbose mode disabled " << std::endl; }

  ros::init(argc, argv, "fps_trajectory_loop_closer_node2");
  ros::NodeHandle n;
  while(ros::Time::now() == ros::Time(0));
  tf::TransformListener* listener = new tf::TransformListener(ros::Duration(120.0));
  tf::TransformBroadcaster* broadcaster = new tf::TransformBroadcaster;

  QApplication* app = new QApplication(argc, argv);
  TrajectoryLoopCloserNodeViewer2* viewer = new TrajectoryLoopCloserNodeViewer2(&trajectory_loop_closer);
  viewer->init(n, listener, broadcaster);
  if(enable_viewer) { viewer->show(); }

  if(verbose) { trajectory_loop_closer.setVerbose(true); }

  const BinaryNodeRelation* previousRelation = 0;    
  while(ros::ok()) {
    ros::spinOnce();      
    app->processEvents();

    if(viewer->lastRelation() != previousRelation) {
      trajectory_loop_closer.findClosures(viewer->lastLocalMap(), &viewer->nodes);
      viewer->addClosuresAndOptimizeMap();
      previousRelation = viewer->lastRelation();
    }
    if (rand() % 10 == 0) viewer->publishTf();
    
    if(output_log != "" && viewer->saveMap()) { viewer->writeMap(output_log); }

    if(enable_viewer && viewer->needRedraw()) { viewer->updateGL(); }
    else { usleep(10000); }
  }
  
  return 0;
}
