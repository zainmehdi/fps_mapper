#pragma once
#include <sys/time.h>
#include <stdexcept>
#include <qapplication.h>
#include <qevent.h>

#include <tf/transform_broadcaster.h>

#include "boss/deserializer.h"
#include "boss/serializer.h"
#include "fps_core/nn_aligner.h"
#include "fps_mapper/MapUpdateMsg.h"
#include "fps_ros_bridge/local_map_viewer.h"
#include "globals/system_utils.h"
#include "fps_global_optimization/g2o_bridge.h"

#include "fps_loop_closer/trajectory_loop_closer.h"
#include "fps_loop_closer/trajectory_energy_morphing_calculator.h"

namespace fps_mapper {

  class TrajectoryLoopCloserNodeViewer2: public LocalMapViewer {
  public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW;

    TrajectoryLoopCloserNodeViewer2(TrajectoryLoopCloser* trajectory_loop_closer_,
				    boss::IdContext* context_ = 0);

    void init(ros::NodeHandle& n, tf::TransformListener* listener, tf::TransformBroadcaster* broadcaster = 0);

    void keyPressEvent(QKeyEvent *e) ;
    
    virtual void onNewLocalMap(LocalMap* lmap);
    virtual void onNewNode(MapNode* n);
    virtual void onNewRelation(BinaryNodeRelation* r) ;
    virtual void onNewCameraInfo(BaseCameraInfo* cam) ;
    virtual void draw();

    void optimizeMap(int iterations);
    void addClosuresAndOptimizeMap();

    void publishUpdates();
    void publishTf();

    void writeMap(std::string output_log);

    inline bool saveMap() const { return _save_map; }
    inline const BinaryNodeRelation* lastRelation() const { return _last_relation; }
    inline LocalMap* lastLocalMap() { return _last_local_map; }
    inline const TrajectoryLoopCloser* trajectoryLoopCloser() const { return _trajectory_loop_closer; }

    inline void setTrajectoryLoopCloser(TrajectoryLoopCloser* trajectory_loop_closer_) { _trajectory_loop_closer = trajectory_loop_closer_; }

  protected:
    bool _save_screenshots, _save_map;
    int _counter, _id;
    std::string _output_log;

    Eigen::Isometry3f _last_tracker_transform;
    G2OBridge _g2o_bridge;
    MapNode* _last_node;
    BinaryNodeRelation* _last_relation;
    LocalMap* _last_local_map;
    TrajectoryLoopCloser* _trajectory_loop_closer;

    tf::TransformBroadcaster* _tf_broadcaster;
    ros::Publisher _updates_pub;    

    std::list<boss::Serializable*> _serializable_objects;
    BinaryNodeRelationSet _added_closures;
  };

}
