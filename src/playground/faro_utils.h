#pragma once
#include "fps_globals/defs.h"
namespace fps_mapper {
  

  void loadFaroFile(Vector3fVector& points, 
		    std::vector<int>& intensities,
		    std::istream& is);

  void points2sphericalImage(UnsignedShortImage& depth,
			     RGBImage& rgb,
			     float horizontal_fov,
			     float vertical_fov,
			     const Vector3fVector& points,
			     const std::vector<int>& intensities);

}
