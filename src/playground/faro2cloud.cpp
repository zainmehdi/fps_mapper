#include "faro_utils.h"
#include <fstream>
#include "fps_core/spherical_projector.h"

using namespace fps_mapper;
using namespace std;

int main(int argc, char** argv){
  int cols=7200;
  int rows=3600;
  float horizontal_fov=2*M_PI;
  float vertical_fov=M_PI;
  ifstream is(argv[1]);
  if (! is){
    cerr << "cant open file";
    return 0;
  }
  cerr << "loading... ";
  Vector3fVector points;
  std::vector<int> intensities;
  loadFaroFile(points, intensities, is);
  cerr << "loaded " << points.size() << " points" << endl;

  cerr << "making spherical image: " << rows << "x" << cols 
       << " hfov:" << horizontal_fov 
       << " vfov:" << vertical_fov<< endl;

  UnsignedShortImage depth;
  RGBImage rgb;
  depth.create(rows,cols);
  rgb.create(rows,cols);
  points2sphericalImage(depth, rgb, horizontal_fov, vertical_fov, points, intensities);
  cerr << "done" << endl;
  SphericalProjector projector;
  projector.setImageSize(rows,cols);
  Eigen::Vector4f camera_matrix;
  camera_matrix << horizontal_fov, vertical_fov, cols/horizontal_fov, rows/vertical_fov;
  projector.setK(camera_matrix);
  projector.setMaxDistance(65);
  Cloud cloud;
  projector.unproject(cloud, depth);
  ofstream os(argv[2]);
  cloud.write(os);
}
